#ifndef ProbModel_H
#define ProbModel_H
#include "util.h"
#include <fstream>
using namespace std;

#define Num_RNAType 4
#define MIN_PROB 1e-6

class ProbChar{
	public:
		ProbChar();
		ProbChar(REAL arr[]);
		
		REAL &operator [] (const int & idx) {return probvect[idx];};
		ProbChar &operator =(ProbChar &pc) 
		   {
		   	for (int i=0;i<=Num_RNAType+2; i++) probvect[i]=pc[i];
		   	return pc;
		  };
		REAL Dist(ProbChar &pc);
		REAL Similar(ProbChar & pc);
		void Assign(REAL arr[]);
		bool isGap() {return (probvect[0]+probvect[Num_RNAType+2]) >=1.0-EPS;};
		bool isUnknown() {return probvect[Num_RNAType+1] >=1.0-EPS;};
		REAL ProbEnd() {return probvect[Num_RNAType+2];};
		REAL ProbGap() {return probvect[0]+probvect[Num_RNAType+2];};
		
		REAL Norm() { 
			REAL res=0;
			for (int i=0;i<=Num_RNAType+2;i++) res+=probvect[i];
			return res;
		}
		
		void operator +=(ProbChar &pc)
			{
		   	for (int i=0;i<=Num_RNAType+2; i++) probvect[i]+=pc[i];
			}; 
		void operator *=(REAL w)
			{
		   	for (int i=0;i<=Num_RNAType+2; i++) probvect[i]*=w;
			}; 
		
		char ToChar();	
		char ToWildcard();
	private:
		REAL probvect[Num_RNAType+3];

};


class ProbString{
	
	public:
	
	ProbString(){len=0;seq=NULL;};
	ProbString(char *strseq);

	~ProbString();

	void Alloc(int len);	
	void Clear();
	void FromString(char *strseq);
	
	ProbChar & operator [] (const int & idx) {return seq[idx];};
	ProbString & operator =(ProbString &ps);
	void operator +=(ProbString &ps); 
	void operator *=(REAL w); 
	void Add(char *str, REAL wt);
	
	int Len() {return len;};
	int DOF();
	
	REAL AveDist(ProbString &str);
	REAL AveDist(char *str);
	REAL MinDist(ProbString &str);
	REAL ProbMaxDist(ProbString &str, REAL pval);
	REAL ProbMaxDist(char *str, REAL pval);
		
	char* ToString();
	char* ToWildcard();
	bool Rectify(REAL thres);
	
	private:
		ProbChar *seq;
		int len;
		int findcutofflen(REAL *difvec,int difcnt,REAL startprob,REAL pval);
};



//template<typename _CharT,class _Traits> basic_ostream<_CharT,_Traits>& operator << ( basic_ostream<_CharT,_Traits> & out, ProbString &str);
fstream & operator <<(fstream &out, ProbChar &chr);
fstream & operator <<(fstream &out, ProbString &str);
fstream &operator >>(fstream &in,ProbChar &chr);
fstream &operator >>(fstream &in,ProbString &str);


extern ProbChar gapChar;
extern ProbChar EndgapChar;

#endif
