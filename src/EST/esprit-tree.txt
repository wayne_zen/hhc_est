#!/bin/bash

if expr index $1 "." >/dev/null; then
prefix=`echo $1|sed -r 's/(.*)(\..*)/\1/g'`
suffix=`echo $1|sed -r 's/(.*)(\..*)/\2/g'`
else
prefix=$1
suffix=""
fi
#echo $prefix
#echo $suffix

if expr index $0 "/" >/dev/null; then
codepath=`echo $0|sed -r 's/(.*\/)(.*)/\1/g'`
else
codepath=""
fi

procfile=$prefix'_Clean'$suffix
frqfile=$prefix'_Clean.frq'
mapfile=$prefix'_Clean.map'
#echo $procfile

if ${codepath}preproc $1 ; then

if [ $# -ge 2 ]; then
${codepath}pbpcluster -o $2 -f $frqfile $procfile
perl ${codepath}invmap.pl $2'.Clusters' $mapfile $2'.org.Clusters'
else 
${codepath}pbpcluster -f $frqfile $procfile
perl ${codepath}invmap.pl $prefix'_Clean.Clusters' $mapfile $prefix'.org.Clusters'
fi

fi

