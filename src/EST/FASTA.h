/***************************************************************************************
ESPRIT: Estimating Species Richness Using Large Collections of 16S rRNA Shotgun Sequences  
Yijun Sun, Yunpeng Cai, Li Liu, Fahong Yu, Michael L. Farrell, William McKendree, William Farmerie
Nucleic Acids Research 2009

Programed by Yunpeng Cai
Copyright Hold by University of Florida, Gainesville, FL32610

****************************************************************************************/
/********************************Reading and Writing FASTA File ************************/

#ifndef FASTA_H
#define FASTA_H
#include <stdio.h>

class FASTA
{
	FILE *fhandle;
	
	public:
	FASTA();
	~FASTA();
	void OpenRead(const char *filename);
	void OpenWrite(const char *filename);	
	void Close();
	int GetFastaSeq(char **label,char ** seq,int del_N,int trim_space=0);
	// label: name of the sequence
	// seq : content of the sequence
	// del_N : if del_N=1, return a null sequence if the sequence contains char 'N'
	// trim_space : if set, delete the spaces in the label
	void WriteFastaSeq(const char *label,const char *seq);
	protected:
		char *CodeTable; // An list of valid characters in the sequence. Chars not in the list 
		// will be automatically deleted. You should initialize CodeTable in the constructor if
		// you override the class
};

#endif
