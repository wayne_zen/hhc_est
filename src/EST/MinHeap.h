#ifndef MIN_HEAP_H
#define MIN_HEAP_H
#include <vector>
#include <algorithm>

using namespace std;


class HeapBase{

public:
		REAL key;
		void *pobject;
		HeapBase(void *pobj, REAL k) {pobject=pobj; key=k; };
		bool operator < (HeapBase &hb) { return (this->key > hb.key);};
		HeapBase & operator = (HeapBase &hb) { this->key=hb.key; this->pobject=hb.pobject; return hb;};
		
};


class MinHeap{
protected:
   vector<HeapBase> hlist;
public:
	 void Add(void *pobj,REAL key) 
	 {
	 		hlist.push_back(HeapBase(pobj,key)); 
	 		push_heap(hlist.begin(),hlist.end());
	 };
	 REAL Pop(void *& pobj)
	 {
	 		pobj =hlist.front().pobject;
	 		REAL res=hlist.front().key;
	 		pop_heap(hlist.begin(),hlist.end()); 
	 		hlist.pop_back();
	 		return res;
	 };	
	 bool Empty() { return hlist.empty();};
     int Size() { return hlist.size();};
	 REAL Top() { return hlist.front().key;};

};
#endif
