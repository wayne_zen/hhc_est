#ifndef DISTCACHE_H
#define DISTCACHE_H
#include "util.h"
#include <string.h>

typedef struct{
int x;
int y;
REAL dist;
}DistRec;

class DistCache{
	DistRec *cells;
	int Size;
	bool rearrange;
	int *alpos;
public:
	DistCache(unsigned long int csize) {
		cells=(DistRec *)Malloc( csize*csize*sizeof(DistRec));
		memset(cells,0,csize*csize*sizeof(DistRec));
		Size=csize;
		rearrange=false;
	};
	~DistCache() { free(cells); if (rearrange) free(alpos);};
	void Store(int x, int y, REAL dist);
	bool Get(int x, int y,REAL &dist);
	void InitArrange(int Nseqs);
	void Register(int UID, int slot){alpos[UID]=slot;};
	bool ReArranged() {return rearrange;};
	void DoReArrange(int Ncls);
	void Update(int clr1,int clr2,int clrnew, int frq1,int frq2,int Dstyle);
};

#endif
