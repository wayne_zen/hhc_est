#include <stdio.h>
#include <stdlib.h>
#include "MinHeap.h"

int main (int argc, char **argv)
{ 
	REAL rlist[100];
	MinHeap heap;
	
	for (int i=0;i<100;i++)
	{
		rlist[i]=rand();
		heap.Add((void *)(rlist+i),rlist[i]);
	}
	REAL *ptr;
	REAL key;
	
	while (!heap.Empty())
	{
		key=heap.Pop((void *&)ptr);
		printf("key %f Ref %f\n",key, *ptr);
	}
}	
