#ifndef UTIL_H
#define UTIL_H
#include <stdlib.h>

#define Sqr(x) ((x)*(x))
#define EPS 1e-100

//#ifdef __cplusplus
//extern "C" {
//#endif


void* Malloc(size_t ssize);

inline int Min(int a,int b)
{
	return (a<b)?a:b;
}

inline int Max(int a,int b)
{
	return (a>b)?a:b;
}

inline double Min(double a,double b)
{
	return (a<b)?a:b;
}

inline double Max(double a,double b)
{
	return (a>b)?a:b;	
}

void AddSuff(char *output,char *input, const char *suff);
void RepExt(char *output, const char *ext);

//#ifdef __cplusplus
//}
//#endif

#endif
