
#ifndef KMER_H
#define KMER_H

#include "ProbModel.h"

class Kmer
{
	private:
	int CodeLen;
	int KmerLen;
	int TableLen;

	int KmerIndex(char *s);	
	int KmerIndexNext(int lastidx, char *s);

	void CodeNorm(ProbChar &pc,REAL *vect);
	void CodeFirst(ProbString &seq,REAL *code,int *nonz);
	void ShiftCode(REAL *code1,int *nonz1,REAL *probv,REAL *code2,int *nonz2);
	void AddCode(REAL *res,REAL *code,int *nonz);
	
	public:
	Kmer(int klen,int clen =4); //the length of the k-mer pattern and the number of character types 
	~Kmer();
	int GetKmerLen();  // = pow(clen,klen);
	
	int *AllocCodeTable();
	REAL* AllocCodeTableP();
	void FreeCodeTable(REAL *ptr) {free(ptr);};

	int debug;	
	REAL *KmerCount(ProbString & seq);  
	void KmerCount(char *seq, int *tab, int *kseq);  
	REAL KmerComp(int *tab1, int *tab2, int *kseq1, int seqlen);
	REAL KmerComp(int *tab1, REAL *tab2, int *kseq1,int seqlen,int seqlen2);

	REAL KmerComp(REAL *tab1, REAL *tab2);
	REAL NormalizedKmerComp(REAL *tab1, REAL *tab2);
	REAL* KmerAdd(int *tab1,int *tab2,int frq1,int frq2);
	REAL* KmerAdd(int *tab1,REAL *tab2,int frq1,int frq2);
	REAL* KmerAdd(REAL *tab1,REAL *tab2,int frq1,int frq2);
	
	REAL *DupCode(REAL *code);
	
	void FreeCodeTable(int *ptr) {free(ptr);};
	protected:
	virtual int KmerCode(char c);

};

#endif
