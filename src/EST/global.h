#ifndef GLOBAL_H
#define GLOBAL_H
#include <stdio.h>
#include "kmer.h"
#include "needle.h"
#include "ProbCluster.h"
#include "distcache.h"

#define DS_Ave 0
#define DS_Max 1


typedef struct{
	int idx;
	REAL dist;
}KDist;


class Global{
public:
	static REAL gap_o;
	static REAL gap_e;
	static int Needle_Diag;
	static int Kmer_Len;
	static REAL *krate;
	static int num_krate;
	static REAL krate_step;
	static REAL DiagRate;
	static int CacheSize;
	
	static Kmer *kmer;
	static Needle *needle;
	static Tree ** childlist;
	static KDist *kdistlist;
	static DistCache* cache;
	
	static REAL level_min;
	static REAL level_inc;
	static REAL level_max;
	static REAL level_step;
	static REAL ErrorRate;
	static REAL currentlevel;
	static bool UseApprox;
	static bool ShowAlign;
	
	static int Dist_Style;
	static char *buf1;
	static char *buf2;
	static char *ProbSeqOut;

	static void SetParam(REAL g_o,	REAL g_e, int K_Len, int K_Br);
	static void Init(int SeqNum);
	static void Finish();
	static void LoadKList(char *filename);
};

int CompDist(const void * a, const void * b);
REAL KdistBound(REAL dist);
REAL calcDistance(char *seq1, char *seq2);
bool QuickDist(int id1,int id2, int query,int newid, int Sq1, int Sq2, REAL &dist);

#define Max_StrLen 65535

extern char **SeqStrs;
extern int *SeqLens;
extern int **KmerTabs;
extern int **KmerSeqs;
extern REAL **KmerCodes;
extern int SeqNum;
extern int SumFreq;
extern char **AlignedSeqs;


#endif
