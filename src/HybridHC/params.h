/*
 * params.h
 *
 *  Created on: Nov 19, 2014
 *      Author: qimao
 */

#ifndef PARAMS_H_
#define PARAMS_H_

#include <tclap/CmdLine.h>
#include <string>

class Params {

protected:
	TCLAP::ValueArg<int> *p;

	TCLAP::ValueArg<int> *k;
	TCLAP::ValueArg<int> *maxiter;
	TCLAP::ValueArg<float> *relative_mse;
	TCLAP::ValueArg<int> *nsample;

	TCLAP::ValueArg<int> *size_cluster;
	TCLAP::ValueArg<float> *diameter;
	TCLAP::ValueArg<int> *distance;

	TCLAP::ValueArg<std::string> *input_filename;
	TCLAP::ValueArg<std::string> *output_filename;

	void add_command(TCLAP::CmdLine &cmd);
public:
	Params(int argc, char *argv[])
	{
		try
		{
			TCLAP::CmdLine cmd("Command description message", ' ', "1.0");
			add_command(cmd);
			cmd.parse( argc, argv );
		} catch (TCLAP::ArgException &e)
		{
			std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		}
	}

	virtual ~Params()
	{
		delete p;

		delete k;
		delete maxiter;
		delete relative_mse;
		delete nsample;

		delete size_cluster;
		delete diameter;
		delete distance;

		delete input_filename;
		delete output_filename;
	}

	int get_numofthreads() { return p->getValue();}

	int get_numofclusters(){ return k->getValue();}
	float get_relative_mse(){ return relative_mse->getValue();}
	int get_maxiter(){return maxiter->getValue();}
	int get_nsample() {return nsample->getValue();}

	int get_size_cluster(){return size_cluster->getValue();}
	float get_diameter(){return diameter->getValue();}
	int get_distance_option(){return distance->getValue();}

	std::string get_input_filename(){ return input_filename->getValue();}
	std::string get_output_filename(){ return output_filename->getValue();}
};

#endif /* PARAMS_H_ */
