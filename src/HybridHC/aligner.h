/*
 * BandNWAlign.h
 *
 *  Created on: Nov 16, 2014
 *      Author: qimao
 */

#ifndef ALIGNER_H_
#define ALIGNER_H_

#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/seq_io.h>

typedef int TValue;
typedef seqan::Score<TValue, seqan::ScoreMatrix<seqan::Dna, seqan::Default> > TScoringSchema;

/*
 *  Abstract class for computing the pairwise distance of an alignment pair sequences
 */
class Aligner
{
public:
	Aligner(){}
	virtual double pairwise_distance(seqan::DnaString &seqH, seqan::DnaString &seqV)=0;
	virtual ~Aligner(){}
};

/*
 *  pairwise banded needleman-wunsch global alignment distance
 */
class BandNWAligner : public Aligner
{
private:

	static int const TAB_SIZE = 16;
	static int const SCORE_MATRIX[TAB_SIZE];
	static int const gap_open_score = -8;
	static int const gap_extend_score = -1;

	double band_ratio;
	TScoringSchema *scoringSchema;

public:
	BandNWAligner(double band_ratio);
	double pairwise_distance(seqan::DnaString &seqH, seqan::DnaString &seqV);
	virtual ~BandNWAligner(){ delete this->scoringSchema;}
};

class KmerAligner : public Aligner
{
private:
	double kmer_window_size;

public:
	KmerAligner(double window_size){this->kmer_window_size = window_size;}
	virtual ~KmerAligner(){}
	double pairwise_distance(seqan::DnaString &seqH, seqan::DnaString &seqV);
};

#endif /* ALIGNER_H_ */
