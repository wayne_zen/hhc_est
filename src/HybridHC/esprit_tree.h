/*
 * run_esprit_tree.h
 *
 *  Created on: Dec 19, 2014
 *      Author: qimao
 */

#ifndef RUN_ESPRIT_TREE_H_
#define RUN_ESPRIT_TREE_H_

#include <seqan/sequence.h>
#include <seqan/stream.h>
#include <seqan/seq_io.h>

#include <unordered_set>
#include <string>
#include <vector>

void esprit_tree(std::vector<std::unordered_set<int> > &clusters, seqan::StringSet<seqan::DnaString> &seqs,
		std::string &fasta_filename, int num_proc, std::string &output_path, std::string &exe_path);

void test_esprit_tree();

#endif /* RUN_ESPRIT_TREE_H_ */
