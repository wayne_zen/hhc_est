/*
 * params.cpp
 *
 *  Created on: Nov 19, 2014
 *      Author: qimao
 */

#include "params.h"

void Params::add_command(TCLAP::CmdLine &cmd)
{
	p = new TCLAP::ValueArg<int>("p","numofthreads","the number of threads in OpenMP (default 1)",false, 1,"int");
	cmd.add(p);

	// -k 2 or --numofclusters 2
	k = new TCLAP::ValueArg<int>("k","numofclusters","the number of clusters for k-means (default 2)",false, 2,"int");
	cmd.add(k);

	// -m
	maxiter = new TCLAP::ValueArg<int>("m","maxiter","the maximum number of iterations for k-means (default 20)",false, 20,"int");
	cmd.add(maxiter);

	// -r
	relative_mse = new TCLAP::ValueArg<float>("r","rmse","the relative mean square error for k-means (default 0.01)",false, 0.01,"float");
	cmd.add(relative_mse);

	// -n
	nsample = new TCLAP::ValueArg<int>("n","numofrepeats","the number of repeats for k-means (default 20)",false, 20,"int");
	cmd.add(nsample);

	// -s
	size_cluster = new TCLAP::ValueArg<int>("s","sizecluster","the size of clusters for active hierarchical clustering (default 10000)",false, 10000,"int");
	cmd.add(size_cluster);

	// -d
	diameter = new TCLAP::ValueArg<float>("d","diameter","the diameter of clusters for active hierarchical clustering (default 0.1)",false, 0.1, "float");
	cmd.add(diameter);

	// -e
	distance = new TCLAP::ValueArg<int> ("e","distance","the option of distance (default 0):\n 0-kmer distance\n 1-banded needleman wunsch\n",false, 0, "int");
	cmd.add(distance);

	// -i
	input_filename = new TCLAP::ValueArg<std::string>("i","inputfilename","the input filename with fasta format",true, "", "string");
	cmd.add(input_filename);

	// -o
	output_filename = new TCLAP::ValueArg<std::string>("o","outputdirectory","the output directory with clusters",false, "", "string");
	cmd.add(output_filename);
}
