c_dir=$(pwd)
cd $c_dir/src/EST/
make
cd $c_dir/src/HybridHC/
make
cd $c_dir/ 
cp src/EST/preproc src/EST/pbpcluster src/HybridHC/hybridhc EXE
cd $c_dir/example/
../exe/hybridhc  -p 4 -d 0.3 -s 10000 -e 0 -i test.fa -o .
cd $c_dir/example/
echo 'NMI score with cut-off from 0.01 to 0.10 at genus level'
python nmi.py test.fa test.tax 6 0.01
python nmi.py test.fa test.tax 6 0.02
python nmi.py test.fa test.tax 6 0.03
python nmi.py test.fa test.tax 6 0.04
python nmi.py test.fa test.tax 6 0.05
python nmi.py test.fa test.tax 6 0.06
python nmi.py test.fa test.tax 6 0.07
python nmi.py test.fa test.tax 6 0.08
python nmi.py test.fa test.tax 6 0.09
python nmi.py test.fa test.tax 6 0.10
echo 'NMI score with cut-off from 0.01 to 0.10 at species level'
python nmi.py test.fa test.tax 7 0.01
python nmi.py test.fa test.tax 7 0.02
python nmi.py test.fa test.tax 7 0.03
python nmi.py test.fa test.tax 7 0.04
python nmi.py test.fa test.tax 7 0.05
python nmi.py test.fa test.tax 7 0.06
python nmi.py test.fa test.tax 7 0.07
python nmi.py test.fa test.tax 7 0.08
python nmi.py test.fa test.tax 7 0.09
python nmi.py test.fa test.tax 7 0.10
cd $c_dir
